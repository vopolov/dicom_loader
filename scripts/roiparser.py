import os
import re
from typing import List, Dict, Union

import pydicom
import objc
# NSCoding = objc.protocolNamed('NSCoding')
from Cocoa import NSObject, NSUnarchiver, NSData, NSRectFromString, NSPointFromString, NSStringFromPoint, NSImage


class MyPoint(NSObject):
    def init(self):
        self = super(MyPoint, self).init()
        return self

    def initWithCoder_(self, coder):
        self = super(MyPoint, self).init()

        self.pt = NSPointFromString(coder.decodeObject())

        return self

    @property
    def x(self):
        return self.pt.x

    @property
    def y(self):
        return self.pt.y


class RGBColor:
    def __init__(self):
        self.red = None
        self.green = None
        self.blue = None


# enum ToolMode
tWL, tTranslate, tZoom, tRotate, tNext, tMesure, tROI, t3DRotate, tCross, tOval, tOPolygon, tCPolygon, tAngle, tText,\
    tArrow, tPencil, t3Dpoint, t3DCut, tCamera3D, t2DPoint, tPlain, tBonesRemoval, tWLBlended, tRepulsor, tLayerROI,\
    tROISelector, tAxis, tDynAngle, tCurvedROI, tTAGT, = list(range(30))


class ROI(NSObject):
    """
    Class used to read binary data from ROI dicom file.
    Mimics horos/osirix ROI class
    """
    def init(self):
        self = super(ROI, self).init()
        return self

    def initWithCoder_(self, coder):
        self = super(ROI, self).init()

        if not self:
            return

        fileVersion = coder.versionForClassName_('ROI')
        self.points = coder.decodeObject()
        self.rect = NSRectFromString(coder.decodeObject())
        self.type = coder.decodeObject().floatValue()

        if self.type == 31 or self.type == 30:
            del self
            return

        self.needQuartz = coder.decodeObject().floatValue()
        self.thickness = coder.decodeObject().floatValue()
        self.fill = coder.decodeObject().floatValue()
        self.opacity = coder.decodeObject().floatValue()
        self.color = RGBColor()
        self.color.red = coder.decodeObject().floatValue()
        self.color.green = coder.decodeObject().floatValue()
        self.color.blue = coder.decodeObject().floatValue()
        self.name = coder.decodeObject()
        self.comments = coder.decodeObject()
        self.pixelSpacingX = coder.decodeObject().floatValue()
        self.imageOrigin = NSPointFromString(coder.decodeObject())

        if fileVersion >= 2:
            self.pixelSpacingY = coder.decodeObject().floatValue()
        else:
            self.pixelSpacingY = self.pixelSpacingX

        if self.type == tPlain:
            self.textureWidth = coder.decodeObject().intValue()
            coder.decodeObject().intValue()
            self.textureHeight = coder.decodeObject().intValue()
            coder.decodeObject().intValue()

            self.textureUpLeftCornerX = coder.decodeObject().intValue()
            self.textureUpLeftCornerY = coder.decodeObject().intValue()
            self.textureDownRightCornerX = coder.decodeObject().intValue()
            self.textureDownRightCornerY = coder.decodeObject().intValue()

            try:
                pointerBuff = coder.decodeObject().bytes()
                self.textureBuffer = [pointerBuff[i+j*self.textureWidth] for j in range(self.textureHeigth)
                                      for i in range(self.textureWidth)]
            except objc.error as err:
                pass

        if fileVersion >= 3:
            self.zPositions = coder.decodeObject()
        else:
            self.zPositions = []

        if fileVersion >= 4:
            self.offsetTextBox_x = coder.decodeObject().floatValue()
            self.offsetTextBox_y = coder.decodeObject().floatValue()
        else:
            self.offsetTextBox_x = 0
            self.offsetTextBox_y = 0

        if fileVersion >= 5:
            self._calciumThreshold = coder.decodeObject().intValue()
            self._displayCalciumScoring = coder.decodeObject().boolValue()

        if fileVersion >= 6:
            self.groupID = coder.decodeObject().doubleValue()
            if self.type == tLayerROI:
                self.layerImageJPEG = coder.decodeObject()
                self.layerImageJPEG = NSImage.alloc().initWithData_(self.layerImageJPEG)
                # remove texture from display
                # while( [ctxArray count]) [self deleteTexture: [ctxArray lastObject]];
            self.textualBoxLine1 = coder.decodeObject()
            self.textualBoxLine2 = coder.decodeObject()
            self.textualBoxLine3 = coder.decodeObject()
            self.textualBoxLine4 = coder.decodeObject()
            self.textualBoxLine5 = coder.decodeObject()

        if fileVersion >= 7:
            self.isLayerOpecityConstant = coder.decodeObject().boolValue()
            self.canColorizeLayer = coder.decodeObject().boolValue()
            self.layerColor = coder.decodeObject()
            self.displayTextualData = coder.decodeObject().boolValue()
        else:
            self.displayTextualData = True

        if fileVersion >= 8:
            self.canResizeLayer = coder.decodeObject().boolValue()

        if fileVersion >= 9:
            self.selectable = coder.decodeObject().boolValue()
            self.locked = coder.decodeObject().boolValue()
        else:
            self.selectable = True
            self.locked = False

        if fileVersion >= 10:
            self.isAliased = coder.decodeObject().boolValue()
        else:
            self.isAliased = False

        if fileVersion >= 11:
            self._isSpline = coder.decodeObject().boolValue()
            self._hasIsSpline = coder.decodeObject().boolValue()
        else:
            self._isSpline = False
            self._hasIsSpline = False

        if fileVersion >= 12:
            savedStudyInstanceUID = coder.decodeObject().copy()

        if fileVersion >= 13 and self.type == 31:
            ovalAngle1 = coder.decodeObject().doubleValue()
            ovalAngle2 = coder.decodeObject().doubleValue()

        if fileVersion >= 14:
            roiRotation = coder.decodeObject().doubleValue()

        if fileVersion >= 15:
            zLocation = coder.decodeObject().doubleValue()

        return self


def load_buffer_from_file(file_path: str) -> bytes:
    """
    Loads binary array with comments from dicom file
    :param file_path:
    :return Encapsulated Document tag value:
    """
    file_path = os.path.abspath(file_path)
    assert os.path.isfile(file_path), 'File not found'
    ds = pydicom.read_file(file_path)
    assert ds is not None, 'Can\'t read dicom file'
    assert hasattr(ds, 'EncapsulatedDocument'), 'No \'Encapsulated Document\' tag'
    return ds.EncapsulatedDocument


def parse_buffer(buffer: bytes) -> List[ROI]:
    """
    Loads binary data into ROI instances
    :param buffer:
    :return list of ROI objects:
    """
    nsdata = NSData.alloc().initWithBytes_length_(buffer, len(buffer))
    nsarray = NSUnarchiver.unarchiveObjectWithData_(nsdata)
    return nsarray


def roi_info(roi: ROI) -> Union[None, Dict]:
    """
    Returns shape and comment from ROI instance
    :param roi:
    :return roi parameters in dict:
    """
    if not hasattr(roi, 'points') or not hasattr(roi, 'name'):
        return
    points = [(p.x, p.y) for p in roi.points]
    if not points:
        return
    birads_comment = roi.name
    birads = re.search(r'\d', birads_comment)
    if not birads:
        return
    birads = int(birads.group(0))
    return {'points': points, 'birads': birads}


def get_comments_from_file(file_path: str) -> List[Dict]:
    """
    Returns all comments from dicom file
    :param file_path:
    :return dicom comments:
    """
    buffer = load_buffer_from_file(file_path)
    comments = parse_buffer(buffer)
    comments = [roi_info(c) for c in comments]
    return [c for c in comments if c]


if __name__ == '__main__':
    path = 'test.dcm'
    rois = get_comments_from_file(path)
    for r in rois:
        print(r)
