import os
import pathlib
import zipfile
import tempfile

import numpy as np
import pydicom
import json
from PIL import Image, ImageDraw

from typing import List, Tuple, Dict
from roiparser import get_comments_from_file


def get_dicom_pairs(dicom_dir: str) -> List[Tuple[str, str]]:
    """
    Match dicom images with dicom comments and return file path pairs
    :param dicom_dir:
    :return data:
    """
    dicom_list = [str(p) for p in pathlib.Path(dicom_dir).glob('**/*.dcm')]
    images = {}
    comments = {}
    for dicom_path in dicom_list:
        ds = pydicom.read_file(dicom_path)
        if hasattr(ds, 'pixel_array'):
            uid = ds.SOPInstanceUID
            images[uid] = dicom_path
        elif hasattr(ds, 'ContentSequence'):
            try:
                ref_uid = ds.ContentSequence[0].ReferencedSOPSequence[0].ReferencedSOPInstanceUID
                comments[ref_uid] = dicom_path
            except AttributeError:
                continue
    data = []
    for uid in images:
        if uid in comments:
            data.append((images[uid], comments[uid]))
        else:
            data.append((images[uid], None))
    return data


def draw_mask(comments: List[Dict], width: int, height: int) -> Image:
    """
    Draw mask image blobs, height and width
    :param comments:
    :param width:
    :param height:
    :return mask:
    """
    mask = Image.new('L', (width, height))
    d = ImageDraw.Draw(mask)
    for c in comments:
        points = c['points']
        color = int(c['birads'])
        d.polygon(points, fill=color, outline=color)
    return mask


def process_file(file_path, idx, img_dir, mask_dir):
    """
    Extracts dicom images and comments and saves them to tiffs
    Currently saves images if tif format
    :param file_path:
    :param idx:
    :param img_dir:
    :param mask_dir:
    :return:
    """
    with tempfile.TemporaryDirectory() as tempdir:
        with zipfile.ZipFile(file_path, 'r') as file:
            file.extractall(os.path.abspath(tempdir))

        pairs = get_dicom_pairs(tempdir)
        for pair in pairs:
            img_path, comment_path = pair

            img_ds = pydicom.read_file(img_path)

            if hasattr(img_ds, 'SeriesDescription'):
                orientation = img_ds.SeriesDescription
            else:
                orientation = 'UN'

            basename = '{:04d}_{}'.format(idx, orientation)
            img_path = os.path.join(img_dir, basename + '_img.tiff')
            mask_path = os.path.join(mask_dir, basename + '_mask.png')

            if os.path.isfile(img_path) and os.path.isfile(mask_path):
                continue

            img = img_ds.pixel_array
            img = Image.fromarray(img)

            if comment_path:
                comments = get_comments_from_file(comment_path)
                mask = draw_mask(comments, img.width, img.height)
            if comment_path:
                img.save(img_path, tiffinfo={270: json.dumps(comments)})
                mask.save(mask_path)
            else:
                img.save(img_path)


if __name__ == '__main__':
    root_path = r'/Users/vik/Google Drive File Stream/Мой диск/BreastCancerImg'
    root_path = pathlib.Path(root_path)
    assert root_path.is_dir()
    zip_iter = root_path.glob('**/*.zip')

    img_dir = '../images'
    mask_dir = '../masks'
    os.makedirs(img_dir, exist_ok=True)
    os.makedirs(mask_dir, exist_ok=True)

    zip_iter = sorted(list(zip_iter))
    count = len(zip_iter)

    for idx, file in enumerate(zip_iter):
        print('{:04d} {}'.format(idx, file))
        process_file(file, idx, img_dir, mask_dir)
        print('Processed {:4d} out of {:4d}'.format(idx + 1, count))
