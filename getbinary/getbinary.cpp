//
//  getbinary.cpp
//  roiparser
//
//  Created by Viktoria on 28/08/2019.
//

#include "dcmtk/config/osconfig.h"
#include "dcmtk/dcmdata/dctk.h"
#include <fstream>
#include <iostream>

using namespace std;


int main()
{
    DcmFileFormat fileformat;
    OFCondition status = fileformat.loadFile("test.dcm");
    if (status.good())
    {
        const uint8_t* buffer;
        unsigned long length;
        
        if (fileformat.getDataset()->findAndGetUint8Array(DCM_EncapsulatedDocument, buffer, &length, OFFalse).good())
        {
            std::ofstream ofile("binary_roi", std::ios::binary);
            ofile.write((char*)buffer, length);
            std::cout << "Parsed binary roi" << std::endl;
        }
        
        return 0;
    }
}
